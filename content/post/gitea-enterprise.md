---
date: 2024-03-07T08:00:00+00:00
authors:
  - "lunny"
  - "techknowlogick"
  - "wolfogre"
title: "CommitGo Launches Gitea Enterprise"
tags: ["announcement"]
coverImage: /demos/gitea-enterprise/cover.png
---

CommitGo, a company founded by members of the Gitea Technical Oversight Committee, is happy to announce the release of Gitea Enterprise.

Gitea Enterprise is a premium offering designed to take your collaboration and code management to new heights.

## What's Inside Gitea Enterprise

1. **Branch Protection Inheritance**:
   With branch protection inheritance features, you keep your codebase secure.

2. **Dependency Scanning**:
   Identify and address vulnerabilities in project open source dependencies automatically.

3. **IP Allowlist**:
   Control access to your repositories by specifying approved IP addresses.

4. **Enterprise Themes**:
   Customize your Gitea experience with themes tailored for enterprises.

5. **Mandatory 2FA Authentication**:
   Strengthen account security with mandatory two-factor authentication.

6. **Audit Log**:
   Gain insights into user actions and system events with a comprehensive audit log.

7. **SAML Integration**:
   Simplify user authentication and authorization with seamless SAML integration.

8. Consistent Release Schedule and Long-Term Support versions

To explore the enhanced experience of Gitea Enterprise, visit the [Enterprise page](https://about.gitea.com/products/gitea-enterprise). You can also contact [CommitGo](mailto:support@gitea.com) if you have any questions or need assistance.

## FAQ

- Does this mean Gitea is now Open-Core?

No, the Gitea project governance charter prohibits the inclusion of proprietary code, and we adhere to the project standards. Gitea Enterprise is an offering of CommitGo, not the Technical Oversight Committee of Gitea itself.

- How come this isn't in Gitea itself?

Functionality previously commissioned for development by customers may include internal information. Submission to the Gitea project must be vetted before submission to ensure no leak of sensitive information or trade secrets. This model has already allowed CommitGo to build and contribute the widely-used Gitea Actions functionality.

- How will this change Gitea development?

It won't. The Gitea project has a robust governance charter to ensure that the project is self-sufficient, and this does not propose any changes to it. Each subscription includes a support contract to ensure that reports or support requests do not overwhelm the community tracker.

Thank you for being part of the Gitea journey. We look forward to serving you even more to ensure your success.
