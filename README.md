# Gitea: Blog

[![Join the chat at https://img.shields.io/discord/322538954119184384.svg](https://img.shields.io/discord/322538954119184384.svg)](https://discord.gg/Gitea)

## Hosting

This page is hosted on our infrastructure within Docker containers, it gets
automcatically updated on every push to the `main` branch.

## Install

This website uses the [Docusaurus](https://docusaurus.io/) static site
generator. If you are planning to contribute you'll want to install
Docusaurus on your local machine.

The installation of Docusaurus is out of the scope of this document, so please take
the [official install instructions](https://docusaurus.io/docs/installation) to
get Docusaurus up and running.

## Development

To generate the website and serve it on [localhost:1313](http://localhost:1313)
just execute this command and stop it with `Ctrl+C`:

```
npm run start
```

When you are done with your changes just create a pull request, after merging
the pull request the website will be updated automatically.

## Contributing

Fork -> Patch -> Push -> Pull Request

## Authors

* [Maintainers](https://github.com/orgs/go-gitea/people)
* [Contributors](https://github.com/go-gitea/blog/graphs/contributors)

## License

This project is under the Apache-2.0 License. See the [LICENSE](LICENSE) file
for the full license text.

## Copyright

```
Copyright (c) 2016 The Gitea Authors <https://about.gitea.com>
```
